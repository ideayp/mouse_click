import sys

from PyQt5.QtWidgets import QApplication
import mouse_action


if __name__ == '__main__':
    app = QApplication(sys.argv)
    mainWindow = mouse_action.MainWindow()
    # 实例化键盘
    mainWindow.show()
    sys.exit(app.exec_())
