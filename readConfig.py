import configparser
import os

fp_dir = os.getcwd()
cfgPath = os.path.join(fp_dir, "config.ini")

class ReadConfig:
    def __init__(self):
        self.cf = configparser.ConfigParser()
        self.cf.read(cfgPath)

    def get_url(self):
        return self.cf.get("link", "url")

    def get_user(self):
        return self.cf.get("account", "sleep_time")

    def get_password(self):
        return self.cf.get("account", "password")


readConfig = ReadConfig()

