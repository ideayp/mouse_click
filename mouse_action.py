from ui import Ui_MainWindow
from PyQt5 import QtCore
from PyQt5.QtWidgets import QMainWindow
from pynput.keyboard import Key, Listener
from pymouse import *
import threading
import time
import readConfig

times = [0.01, 0.02, 0.05, 0.1, 0.2, 0.5, 1, 2]

keys = [Key.f1, Key.f2, Key.f3, Key.f4, Key.f5, Key.f6, Key.f7, Key.f8, Key.f9, Key.f10, Key.f11, Key.f12]


def combo_box_key(combo_box, _translate):
    """
    可选择的key
    :param combo_box:
    :param _translate:
    :return:
    """
    for index, value in enumerate(keys):
        combo_box.addItem(str(value.name.capitalize()))


def combo_box_time(combo_box, _translate):
    """
    设置默认 可选择的参数
    :param combo_box:
    :param _translate:
    :return:
    """
    for index, value in enumerate(times):
        combo_box.addItem(str(value))


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

        # 开始监听,按esc退出监听
        t = threading.Thread(target=self.start_listen)
        t.setDaemon(True)
        t.start()
        self.is_click = False
        self.setupUi(self)
        # 默认值

        self.sleep_time = float(readConfig.ReadConfig().get_user())
        self.hot_key = 'F8'
        self.click_count = 0
        _translate = QtCore.QCoreApplication.translate

        # 默认选中
        combo_box_time(self.comboBox, _translate)
        combo_box_key(self.comboBox_2, _translate)

        self.comboBox.setCurrentText(str(self.sleep_time))
        self.comboBox_2.setCurrentText(str(self.hot_key))
        # 事件
        self.comboBox.currentIndexChanged['QString'].connect(self.change_time)
        self.comboBox_2.currentIndexChanged['QString'].connect(self.change_key)
        self.pushButton.clicked.connect(self.start_click_btn)

        self.radioButton.setChecked(True)
        self.radioButton.toggled.connect(self.change_btn)
        self.change_show()

    def change_time(self, value):
        try:
            self.sleep_time = float(value)
        except Exception:
            self.sleep_time = 1
        self.change_show()

    def change_key(self, value):
        try:
            self.hot_key = value
        except Exception:
            self.hot_key = 'F8'
        self.change_show()

    def change_btn(self):
        self.change_show()

    def change_show(self):
        _translate = QtCore.QCoreApplication.translate
        self.pushButton.setText(_translate("MainWindow", self.get_title()))

    def get_title(self):
        if self.radioButton.isChecked():
            a = '鼠标左键'
        else:
            a = '鼠标右键'
        return '按' + self.comboBox_2.currentText() + '开始' + a + str(self.click_count)

    def start_click_btn(self):
        threading.Thread(target=self.run_click).start()

    def start_click(self):
        self.is_click = True
        mouse = PyMouse()
        self.click_count = 0
        if self.radioButton.isChecked():
            button = 1
        else:
            button = 2
        while self.is_click:
            mouse.click(*mouse.position(), button=button)
            self.click_count += 1
            self.change_show()
            time.sleep(self.sleep_time)

    def stop_click(self):
        self.is_click = False

    def run_click(self):
        if not self.is_click:
            t1 = threading.Thread(target=self.start_click)
            t1.setDaemon(True)
            t1.start()
        else:
            self.stop_click()

    # 监听按压
    def on_press(self, key):
        try:
            if key.name.capitalize() == self.hot_key:
                self.run_click()
        except AttributeError:
            print("正在按压:", format(key))

    # 开始监听
    def start_listen(self):
        with Listener(on_press=self.on_press) as listener:
            listener.join()
